const { getTokenAuth0, getUsersAuth0, getUserAuth0, getRolesByUserAuth0, getRolesAuth0, addRoleToUserAuth0, removeRoleFromUserAuth0 } = require("../Auth0");

const domain = "dev-272dnyhs.us.auth0.com";
const client_id = "bBwijFiDQYpRV9nIpB0avj9Z0Kt3SBHd";
const secret = "kLSHExtORHeei-hbyWXzpAUQiI8YyYuEOklvJVEp3rgPVyiUL0oDcf98JutZ6BeC";

let access_token = null;
let user = null;
let roles = null;
let userRoles = null;

test('Test get token',  async () => {

    try {
        access_token = await getTokenAuth0(domain, client_id, secret);
    }catch (error) {
        console.error(error);
    }
    expect((typeof access_token === 'string')).toBe(true);
  
}, 60000);

test('Test get users',  async () => {

    let users = null;
    try {
        users = await getUsersAuth0(domain, access_token);
    }catch (error) {
        console.error(error);
    }
    expect(Array.isArray(users)).toBe(true);
    expect((users.length > 0 )).toBe(true);
    user = users[0];
  
}, 60000);

test('Test get user',  async () => {

    let _user = null;
    try {
        _user = await getUserAuth0(domain, access_token, user.user_id);
    }catch (error) {
        console.error(error);
    }
    expect(user.user_id === _user.user_id).toBe(true);
  
}, 60000);

test('Test get roles',  async () => {

    try {
        roles = await getRolesAuth0(domain, access_token);
    }catch (error) {
        console.error(error);
    }
    expect(Array.isArray(roles)).toBe(true);
  
}, 60000);

test('Test get roles from user',  async () => {
    
    try {
        userRoles = await getRolesByUserAuth0(domain, access_token, user.user_id);
    }catch (error) {
        console.error(error);
    }
    expect(Array.isArray(userRoles)).toBe(true);
  
}, 60000);

test('Test add rol to user',  async () => {

    const oldRoles = userRoles.map(item=>item.id);
    oldRoles.push(roles[0].id);
    const body = {
        "roles": oldRoles
      }
    let _roles = null;
    try {
        await addRoleToUserAuth0(domain, access_token, user.user_id, body);
        _roles = await getRolesByUserAuth0(domain, access_token, user.user_id);
    }catch (error) {
        console.error(error);
    }
    expect((userRoles.length <= _roles.length)).toBe(true);
    userRoles = _roles;
  
  
}, 60000);

test('Test delete rol from user',  async () => {

    const deleteRoles = userRoles.map(item=>item.id);
    const body = {
        "roles": deleteRoles
      }
    let _roles = null;
    try {
        await removeRoleFromUserAuth0(domain, access_token, user.user_id, body);
        _roles = await getRolesByUserAuth0(domain, access_token, user.user_id);
    }catch (error) {
        console.error(error);
    }
    expect((userRoles.length  > _roles.length)).toBe(true);
    userRoles = _roles;
  
}, 60000);