const { isPlatform } = require('@ionic/react');
var Auth0Functions = require('./Auth0');
const isMobile = ()=>{
    return ( !isPlatform('mobileweb') && (isPlatform('android') || isPlatform('ios')));
}

module.exports = {
    isMobile,
    Auth0Functions
}





