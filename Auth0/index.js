var axios = require("axios");
var _ = require("underscore");

const getTokenAuth0 = async (domain, client_id, secret) => {
	var options = {
		method: "POST",
		url: `https://${domain}/oauth/token`,
		headers: { "Content-Type": "application/json" },
		data: {
			client_id: client_id,
			client_secret: secret,
			audience: `https://${domain}/api/v2/`,
			grant_type: "client_credentials",
		},
	};

	try {
		const { data } = await axios.request(options);
		return data.access_token;
	} catch (error) {
		console.error(error);
		return false;
	}
};

const getUsersAuth0 = async (domain, token) => {
	try {
		const { data } = await axios.get(`https://${domain}/api/v2/users`, {
			headers: { Authorization: `Bearer ${token}` },
		});
		return data;
	} catch (error) {
		console.error(error);
		return false;
	}
};

const getUserAuth0 = async (domain, token, userId) => {
	try {
		const { data } = await axios.get(`https://${domain}/api/v2/users/${userId}`, {
			headers: { Authorization: `Bearer ${token}` },
		});
		return data;
	} catch (error) {
		console.error(error);
		return false;
	}
};

const editUserAuth0 = async (domain, token, userId, data) => {
	try {
		await axios.patch(`https://${domain}/api/v2/users/${userId}`, data, {
			headers: { Authorization: `Bearer ${token}` },
		});
	} catch (error) {
		console.error(error);
	}
};

const getRolesByUserAuth0 = async (domain, token, userId) => {
	try {
		const { data } = await axios.get(`https://${domain}/api/v2/users/${userId}/roles`, {
			headers: { Authorization: `Bearer ${token}` },
		});
		return data;
	} catch (error) {
		console.error(error);
		return false;
	}
};

const getRolesAuth0 = async (domain, token) => {
	try {
		const { data } = await axios.get(`https://${domain}/api/v2/roles`, {
			headers: { Authorization: `Bearer ${token}` },
		});
		return data;
	} catch (error) {
		console.error(error);
		return false;
	}
};

const addRoleToUserAuth0 = async (domain, token, userId, body) => {
	body.roles = _.uniq(body.roles, false);
	try {
		const { data } = await axios.post(`https://${domain}/api/v2/users/${userId}/roles`, body, {
			headers: { Authorization: `Bearer ${token}` },
		});
		return data;
	} catch (error) {
		console.error(error);
		return false;
	}
};

const removeRoleFromUserAuth0 = async (domain, token, userId, body) => {
	body.roles = _.uniq(body.roles, false);
	try {
		const { data } = await axios.delete(`https://${domain}/api/v2/users/${userId}/roles`, {
			headers: { Authorization: `Bearer ${token}` },
			data: body,
		});

		return data;
	} catch (error) {
		console.error(error);
		return false;
	}
};

module.exports = {
	getRolesAuth0,
	getTokenAuth0,
	addRoleToUserAuth0,
	getUserAuth0,
	removeRoleFromUserAuth0,
	getRolesByUserAuth0,
	getUsersAuth0,
	editUserAuth0,
};
